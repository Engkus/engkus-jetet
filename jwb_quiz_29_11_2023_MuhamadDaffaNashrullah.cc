//Muhamad Daffa Nashrullah

#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main() {

    system("clear");

    int n, i = 1;
    double total = 0.0, nilai;

    cout << "Masukan Data ke" << i << " : ";
    cin >> nilai;

    do {
        cout << "Masukan Data ke" << i << " : ";
        cin >> nilai;
        total += nilai;
        i++;
    } while (nilai != 0);

    n = i - 2;

    double rata_rata = (n == 0) ? 0 : total / n;

    cout << "Banyaknya Data       : " << n << endl;
    cout << "Total Nilai Data     : " << setprecision(2) << total << endl;
    cout << "Rata-rata nilai data : " << setprecision(2) << rata_rata << endl;

    return 0;
}
